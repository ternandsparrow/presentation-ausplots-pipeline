#!/bin/sh
# builds the slideshow
set -euo pipefail
cd `dirname "$0"`

OUT_DIR=output
IN_DIR=src
IMG_DIR=img

rm -fr $OUT_DIR
mkdir -p $OUT_DIR

for f in $IN_DIR/*.md; do
  FILE_NAME=`basename $f | sed 's/.md//g'`
  echo "Processing $f"
  pandoc --standalone -t revealjs $f -o $OUT_DIR/$FILE_NAME.html
done

cp -r $IMG_DIR $OUT_DIR
