% Ausplots data pipeline
% Tom Saleeba
% 23 Feb 2021

### High level view

:::::::::::::: {.columns}
::: {.column width="70%" align="center"}

![](img/high-level.png){width="100%"}

:::
::: {.column width="30%"}

1. collection
1. upload
1. QA and publish
1. consume

:::
::::::::::::::

---

## Collection

The field team go out into the field and use the app to collect data.

---

## Upload

Site visit data collected in the app is *uploaded* to "the cloud".

---

## QA and publish

Data Curator checks data, then publishes it.

---

## Consume

Users of AusplotsR can now download data for this site visit.

---

## What's inside that cloud anyway?

---

![](img/cloud.png)

---

## Couch

  - it's the name of a database software that we use
  - a place for uploaded data from the app to land
  - ingest process (to main database) reads from here

---

## The main database

 - single store of truth
 - contains all our data, guard it with your life!
 - all the data eventually ends up here
 - Data Curator works in this space

---

## Curation UI

  - web page tool
  - shows data in all tables
  - allow editing to correct errors
  - has built-in reports (I think, maybe)

---

![](img/curation-ui.png){width="100%"}

---

## Data Curator

  - asserts visits are complete before publishing
  - has the power to *publish* site visits
  - looks for, and corrects, errors (from humans and computers)

---

## The mirror database

 - this is where AusplotsR gets its data from
 - updated with the latest data every 24 hours
     * this means you *will* get different results on different days!
 - separated from the main database for security reasons

---

## That's it

Any questions? Preferably related to the presentation but I'll have a go at
other things too.

:::::::::::::: {.columns}
::: {.column width="50%" align="center"}

![](img/high-level.png)

:::
::: {.column width="50%"}

![](img/cloud.png)

:::
::::::::::::::
