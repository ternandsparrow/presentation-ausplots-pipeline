> Source to generate slides for this presentation.

See [live deployed
presentation](https://ternandsparrow.gitlab.io/presentation-ausplots-pipeline).

Builds on the example from
https://stymied.medium.com/what-slides-from-markdown-5239ed31e7ac.

Also see the pandoc manual about slides
https://pandoc.org/MANUAL.html#slide-shows.

Uses:

1. pandoc
1. dzslides template

# How to run

1. clone repo
1. make sure `pandoc` is installed
1. `./build.sh`
1. open `./output/index.html` in your browser
